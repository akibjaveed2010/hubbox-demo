package com.hubbox.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;;

// TODO: Auto-generated Javadoc
/**
 * The Class EmailUtility.
 */
@Component
public class HtmlUtility {

	@Autowired
	VelocityUtility velocityUtility;

	/**
	 * Create HTML from Velocity Template.
	 *
	 * @param htmlBody
	 *            the html Body
	 * 
	 */
	public String createHtml(String htmlBody) {

		Map<String, String> props = new HashMap<String, String>();
		props.put("emailBody", htmlBody);

		String mailBody = velocityUtility.getTemplatetoText("mail.tmpl", props);
		return mailBody;

	}
	
	/**
	 * @param SERVER_DETAIL : server url along with application version
	 * @param url : confirm password url
	 * @param token : reset token
	 * @return
	 */
	public String createForgetPasswordTemplate(String SERVER_DETAIL, String url, String token){
		Map<String, String> props = new HashMap<String, String>();
		props.put("SERVER_DETAIL", SERVER_DETAIL);
		props.put("url", url);
		props.put("token", token);

		String mailBody = velocityUtility.getTemplatetoText("forgetPassword.tmpl", props);
		return mailBody;
	}

	/**
	 * @param authmsg : relevant message on failure or success
	 * @return
	 */
	public String createPasswordAuthenticationBody(String authmsg){
		Map<String, String> props = new HashMap<String, String>();
		props.put("authmsg", authmsg);

		String mailBody = velocityUtility.getTemplatetoText("forgetPasswordAuth.tmpl", props);
		return mailBody;
	}

}
