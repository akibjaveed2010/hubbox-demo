package com.hubbox.util;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
/**
 * The Class ApplicationUtility.
 */
@Component
public class ApplicationUtility {

	/** The message source. */
	@Autowired
	MessageSource messageSource;

	public String generateKey() {
		try {
			SecureRandom random = new SecureRandom();
			return new BigInteger(130, random).toString(32);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Gets the message.
	 *
	 * @param key
	 *            the key
	 * @return the message
	 */

	public String getMessage(String key) {
		try {
			return messageSource.getMessage(key, null, Locale.US);
		} catch (Exception e) {
			return null;
		}
	}

	public String getMessage(String key, String[] args) {
		try {
			return messageSource.getMessage(key, args, Locale.US);
		} catch (Exception e) {
			return null;
		}
	}

	public static boolean isEmpty(Object object) {
		if (object != null) {
			if (object instanceof CharSequence) {
				if (StringUtils.isEmpty((CharSequence) object)) {
					return true;
				}
			}
			return false;
		}
		return true;
	}

	public static String convertDate(DateTime dt) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String dtStr = fmt.print(dt);
		return dtStr;
	}

	public static String convertOnlyDate(Date dt) throws ParseException {
		SimpleDateFormat original = new SimpleDateFormat("yyyy-MM-dd");
		String dtStr = original.format(dt);
		return dtStr;
	}

	public static String getCurrentUrl(HttpServletRequest request) throws MalformedURLException, URISyntaxException {
		URL url = new URL(request.getRequestURL().toString());
		String host = url.getHost();
		String userInfo = url.getUserInfo();
		String scheme = url.getProtocol();

		int port = url.getPort();
		String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
		String query = (String) request.getAttribute("javax.servlet.forward.query_string");

		URI uri = new URI(scheme, userInfo, host, port, path, query, null);
		return uri.toString();
	}

}
