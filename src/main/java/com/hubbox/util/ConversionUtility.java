package com.hubbox.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.hubbox.constant.ProviderTypeEnum;
import com.hubbox.constant.UserStatusEnum;
import com.hubbox.rest.model.User;
import com.hubbox.rest.request.model.RegisterDTO;
import com.hubbox.rest.response.model.ResponseEntityDTO;
import com.hubbox.security.JwtTokenUtil;

/**
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@Component
public class ConversionUtility {

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

	@Value("${server.application-path}")
	private String serverApplicationPath;

	@Value("${date.format}")
	private String dateFormat;

	@Value("${datetime.format}")
	private String dateTimeFormat;

	/**
	 * this method converts user depends on registerType
	 * 
	 * @param registerDTO
	 * @param registerType
	 * @return user
	 */
	public User convertUser(RegisterDTO registerDTO, int registerType) {
		User user = new User();
		user.setEmail(registerDTO.getEmail().trim());
		if (registerType == ProviderTypeEnum.ACCOUNT_FACEBOOK.getStatusCode()) {
			user.setStatus(UserStatusEnum.ACTIVE.getStatus());
			user.setProvider(ProviderTypeEnum.ACCOUNT_FACEBOOK.getStatus());
		} else {
			user.setStatus(UserStatusEnum.NEW.getStatus());
			user.setProvider(ProviderTypeEnum.ACCOUNT_NORMAL.getStatus());
		}

		user.setFullName(registerDTO.getFull_name().trim());
		user.setPassword(registerDTO.getPassword().trim());
		user.setUsername(registerDTO.getUsername().trim());
		return user;
	}

	/**
	 * @param httpStatusCodes
	 * @param message
	 * @param body
	 * @return
	 */

	public ResponseEntityDTO createResponseEntityDTO(HttpStatusCodes httpStatusCodes, String message, Object body) {
		return ResponseEntityDTO.response().withResponseCode(httpStatusCodes).withResponseMessage(message)
				.withResponseBody(body).build();
	}

}
