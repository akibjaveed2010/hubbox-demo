package com.hubbox.util;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

/**
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@Component
public class ValidationUtility {

	@Autowired
	@Qualifier("jsr303Validator")
	Validator validator;

	public void validateEntity(final Object object, BindingResult bindingResult) {
		validator.validate(object, bindingResult);
	}

	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}
}
