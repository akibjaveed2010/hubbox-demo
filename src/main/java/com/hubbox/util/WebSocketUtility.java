/*
 * 
 */
package com.hubbox.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * The Class WebSocketUtility.
 */
@Component
public class WebSocketUtility {

	/** The messaging template. */
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	/**
	 * This function is used to send a message to a particular list of user 
	 * that have subscribed to that particular channel
	 *
	 * @param channel the subscribed user channel
	 * @param data the Object that need to be send can be string or any object
	 */
	public void sendMessage(String channel, Object data) {
		messagingTemplate.convertAndSend(channel, data);
	}
}
