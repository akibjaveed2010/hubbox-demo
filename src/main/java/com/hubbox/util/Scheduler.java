package com.hubbox.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class Scheduler {

	@Scheduled(cron = "${cronjob.time}")
	public void printStatement() {
		System.out.println("Calling after 4 seconds");

	}
}
