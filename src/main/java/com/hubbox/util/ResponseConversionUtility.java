package com.hubbox.util;

import java.text.ParseException;

import org.springframework.stereotype.Component;

import com.hubbox.rest.model.User;
import com.hubbox.rest.response.model.LoginDTO;
import com.hubbox.rest.response.model.ResponseEntityDTO;

@Component
public class ResponseConversionUtility {

	public LoginDTO successfullyLoggedIn(User user, String token) throws ParseException {
		return LoginDTO.loggedInUser().withUserId(user.get_id()).withFullName(user.getFullName())
				.withEmail(user.getEmail()).withToken(token).withUsername(user.getUsername())
				.withProfileUrl(user.getAvatar()).build();
	}

	
	public ResponseEntityDTO createResponseEntityDTO(HttpStatusCodes httpStatusCodes, String message, Object body) {
		return ResponseEntityDTO.response().withResponseCode(httpStatusCodes).withResponseMessage(message)
				.withResponseBody(body).build();
	}

}
