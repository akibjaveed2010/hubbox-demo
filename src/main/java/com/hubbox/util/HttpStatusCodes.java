package com.hubbox.util;

/**
 * This class helps to HttpStatus code
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public enum HttpStatusCodes {

	OK(200, "OK"),

	VALIDATION_ERROR(201, "Validation Error"),

	ALREADY_REGISTERED(202, "Already Registered (Active)"),

	NOT_ACTIVATED(203, "Account Not activated"),

	NOT_REGISTERED(204, "Account Not Registered"),

	REGISTERED(205, "User Registered"),

	OTHER_EXCEPTION(301, "Other Exception "),

	BAD_REQUEST(400, "Bad Request"),

	UNAUTHORIZED(401, "Unauthorized"),

	NOT_ACCEPTABLE(406, "Not Acceptable");
	
	private final int value;

	private final String reasonPhrase;

	HttpStatusCodes(int value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}

	public int value() {
		return this.value;
	}

	public String getReasonPhrase() {
		return this.reasonPhrase;
	}

	/**
	 * @param statusCode
	 * @return status
	 */
	public static HttpStatusCodes valueOf(int statusCode) {
		for (HttpStatusCodes status : values()) {
			if (status.value == statusCode) {
				return status;
			}
		}
		throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
	}

}
