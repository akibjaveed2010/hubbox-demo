package com.hubbox.util;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;;

// TODO: Auto-generated Javadoc
/**
 * The Class EmailUtility.
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 * 
 */
@Component
@PropertySource("classpath:mail.properties")
public class EmailUtility {

	/** The mail from. */
	@Value("${mail.from}")
	private String mailFrom;

	@Value("${email.title}")
	private String emailTitle;

	@Autowired
	VelocityUtility velocityUtility;

	/** The java mail sender. */
	private final JavaMailSender javaMailSender;

	/**
	 * Instantiates a new user controller.
	 *
	 * @param javaMailSender
	 *            the java mail sender
	 */
	@Autowired
	EmailUtility(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	/**
	 * Send mail.
	 *
	 * @param emailAddress
	 *            the email address
	 * @param title
	 *            the title
	 * @param link
	 *            the link
	 * @param javaMailSender
	 *            the java mail sender
	 */
	@Async
	public void sendMail(String emailAddress, String emailBody) {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mimeMessage, false, "utf-8");

			helper.setTo(emailAddress);
			helper.setFrom(mailFrom);
			helper.setSubject(emailTitle);

			Map<String, String> props = new HashMap<String, String>();
			props.put("emailBody", emailBody);

			String mailBody = velocityUtility.getTemplatetoText("mail.tmpl", props);

			mimeMessage.setContent(mailBody, "text/html");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		javaMailSender.send(mimeMessage);
	}

}
