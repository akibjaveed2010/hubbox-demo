package com.hubbox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hubbox.config.MailPropertiesConfig;
import com.hubbox.constant.MailNameConstants;
import com.hubbox.util.HttpStatusCodes;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This class is main spring-boot/(HubBox)application
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@SpringBootApplication
@EnableSwagger2
@EnableAsync
@EnableEncryptableProperties
@PropertySource(name="EncryptedProperties", value = "classpath:encrypted.properties")
public class HubboxApplication extends SpringBootServletInitializer implements AsyncConfigurer {

	@Autowired
	MailPropertiesConfig props;

	@Bean
	JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(props.getHost());
		mailSender.setPort(props.getPort());
		mailSender.setUsername(props.getUsername());
		mailSender.setPassword(props.getPassword());

		Properties mailProperties = new Properties();
		mailProperties.put(MailNameConstants.MAIL_AUTH, props.getSmtp().isAuth());
		mailProperties.put(MailNameConstants.MAIL_SOCKET, MailNameConstants.MAIL_SOCKET_VALUE);
		mailProperties.put(MailNameConstants.MAIL_TTS, props.getSmtp().isStarttlsEnable());
		mailSender.setJavaMailProperties(mailProperties);
		mailSender.setProtocol(MailNameConstants.MAIL_PROTOCOL);

		return mailSender;

	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(HubboxApplication.class);
	}

	public static void main(String[] args) throws IOException {
		SpringApplication.run(HubboxApplication.class, args);
	}

	@Bean
	public Docket swaggerSettings() {
		Parameter parameter = new ParameterBuilder().name("Authorization").description("Authorization Token")
				.modelRef(new ModelRef("string")).parameterType("header").required(false).build();
		List<Parameter> parameters = new ArrayList<Parameter>();
		parameters.add(parameter);
		Docket docket = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.hubbox.controller")).paths(PathSelectors.any())
				.build().apiInfo(apiInfo()).pathMapping("/").globalOperationParameters(parameters)
				.useDefaultResponseMessages(false);

		docket.globalResponseMessage(RequestMethod.POST, getResponseCodes())
				.globalResponseMessage(RequestMethod.PUT, getResponseCodes())
				.globalResponseMessage(RequestMethod.GET, getResponseCodes())
				.globalResponseMessage(RequestMethod.DELETE, getResponseCodes());

		return docket;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList<ResponseMessage> getResponseCodes() {
		ArrayList responseCodes = new ArrayList();
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.OK.value())
				.message(HttpStatusCodes.OK.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.VALIDATION_ERROR.value())
				.message(HttpStatusCodes.VALIDATION_ERROR.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.ALREADY_REGISTERED.value())
				.message(HttpStatusCodes.ALREADY_REGISTERED.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.NOT_ACTIVATED.value())
				.message(HttpStatusCodes.NOT_ACTIVATED.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.NOT_REGISTERED.value())
				.message(HttpStatusCodes.NOT_REGISTERED.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.REGISTERED.value())
				.message(HttpStatusCodes.REGISTERED.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.OTHER_EXCEPTION.value())
				.message(HttpStatusCodes.OTHER_EXCEPTION.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.BAD_REQUEST.value())
				.message(HttpStatusCodes.BAD_REQUEST.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.UNAUTHORIZED.value())
				.message(HttpStatusCodes.UNAUTHORIZED.getReasonPhrase()).build());
		responseCodes.add(new ResponseMessageBuilder().code(HttpStatusCodes.NOT_ACCEPTABLE.value())
				.message(HttpStatusCodes.NOT_ACCEPTABLE.getReasonPhrase()).build());
		
		
		return responseCodes;
	}

	@Bean
	public Validator jsr303Validator() {
		return new LocalValidatorFactoryBean();
	}

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo("HubBox REST API", "HubBox Rest API Document for App", "API TOS",
				"Terms of service", "HubBox Pvt Ltd", "License of API", "");
		return apiInfo;
	}

	/**
	 * Gets the message source dev.
	 *
	 * @return the message source dev
	 */
	@Bean(name = "messageSource")
	public MessageSource getMessageSourceDev() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("HubBox-");
		executor.initialize();
		return executor;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return null;
	}

}
