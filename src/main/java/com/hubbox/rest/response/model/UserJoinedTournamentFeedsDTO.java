package com.hubbox.rest.response.model;

import java.util.Date;

public class UserJoinedTournamentFeedsDTO {

	private String tid;

	private String name;

	private Date startDate;

	private Date endDate;

	public String getTid() {
		return tid;
	}

	public String getName() {
		return name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}