package com.hubbox.rest.response.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * this class is helps to login data transfer objects
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@ApiModel(description = "Login Response Body.")
public class LoginDTO {

	@ApiModelProperty(name = "user_id")
	String user_id;
	@ApiModelProperty(name = "token")
	String token;
	@ApiModelProperty(name = "full_name")
	String full_name;
	@ApiModelProperty(name = "email")
	String email;
	@ApiModelProperty(name = "username")
	private String username;
	@ApiModelProperty(name = "profileUrl")
	private String profile_url;

	private LoginDTO(Builder builder) {
		user_id = builder.user_id;
		token = builder.token;
		full_name = builder.full_name;
		email = builder.email;
		username = builder.username;
		profile_url = builder.profile_url;

	}

	public String getUser_id() {
		return user_id;
	}

	public String getToken() {
		return token;
	}

	public String getFull_name() {
		return full_name;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getProfile_url() {
		return profile_url;
	}

	public static Builder loggedInUser() {
		return new Builder();
	}

	public static final class Builder {
		private String user_id;
		private String full_name;
		private String email;
		private String token;
		private String username;
		private String profile_url;

		private Builder() {
		}

		public Builder withProfileUrl(String val) {
			profile_url = val;
			return this;
		}

		public Builder withUsername(String val) {
			username = val;
			return this;
		}

		public Builder withUserId(String val) {
			user_id = val;
			return this;
		}

		public Builder withFullName(String val) {
			full_name = val;
			return this;
		}

		public Builder withEmail(String val) {
			email = val;
			return this;
		}

		public Builder withToken(String val) {
			token = val;
			return this;
		}

		public LoginDTO build() {
			return new LoginDTO(this);
		}

	}
}
