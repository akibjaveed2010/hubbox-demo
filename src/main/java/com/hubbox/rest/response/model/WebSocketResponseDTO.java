package com.hubbox.rest.response.model;

public class WebSocketResponseDTO {

	private String content;

	public WebSocketResponseDTO() {
	}

	public WebSocketResponseDTO(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
}
