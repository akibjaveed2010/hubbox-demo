package com.hubbox.rest.request.model;

import io.swagger.annotations.ApiModel;

/**
 * This class helps to profile data transfer objects
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@ApiModel(description = "User profile Body.")
public class ProfileDTO {

	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipcode;
	private String phoneNumber;
	private String bio;

	public ProfileDTO() {
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public String getBio() {
		return bio;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
