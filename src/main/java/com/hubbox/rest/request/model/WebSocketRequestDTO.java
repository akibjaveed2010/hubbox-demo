package com.hubbox.rest.request.model;

public class WebSocketRequestDTO {

	private String name;

	public WebSocketRequestDTO() {
	}

	public WebSocketRequestDTO(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
