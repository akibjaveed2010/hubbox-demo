package com.hubbox.rest.service;

import java.util.List;

import com.hubbox.rest.model.Role;

/**
 * Created by Aquib
 */
public interface AuthorityService {

	public List<Role> getAuthorityList();
	
	public Role getAuthorityByName(String roleName);

}
