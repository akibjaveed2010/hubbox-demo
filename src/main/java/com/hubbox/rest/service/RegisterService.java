package com.hubbox.rest.service;

import com.hubbox.rest.exception.UserAlreadyExistException;
import com.hubbox.rest.model.User;
import com.hubbox.rest.request.model.RegisterDTO;

/**
 * This is RegisterService interface
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public interface RegisterService {

	/**
	 * Register the user based on register type. RegisterType can be 1 or 2 i.e
	 * AccountTypeConstant
	 * 
	 * @param registerDTO
	 * @param registerType
	 * @return
	 * @throws UserAlreadyExistException
	 */
	public User registerUser(RegisterDTO registerDTO, int registerType) throws UserAlreadyExistException;

}
