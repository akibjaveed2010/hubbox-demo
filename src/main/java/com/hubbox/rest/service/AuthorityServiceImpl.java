package com.hubbox.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hubbox.rest.model.Role;
import com.hubbox.security.repository.AuthorityRepository;
import com.hubbox.util.ConversionUtility;

/**
 * Created by Aquib
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {

	@Autowired
	private AuthorityRepository authorityRepository;

	@Autowired
	ConversionUtility conversionUtility;

	@Override
	public List<Role> getAuthorityList() {
		return authorityRepository.findAll();
	}

	@Override
	public Role getAuthorityByName(String roleName) {
		return authorityRepository.findByName(roleName);
	}
}
