package com.hubbox.rest.service;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hubbox.constant.UserStatusEnum;
import com.hubbox.constant.UserTokenTypeConstant;
import com.hubbox.rest.model.User;
import com.hubbox.rest.model.UserToken;
import com.hubbox.rest.repository.UserTokenRepository;
import com.hubbox.security.service.JwtUserDetailsServiceImpl;
import com.hubbox.util.ApplicationUtility;
import com.hubbox.util.EmailUtility;
import com.hubbox.util.HtmlUtility;

/**
 * This class is ActivationTokenServiceImpl which is implements
 * ActivationTokenService
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@Service
public class ActivationTokenServiceImpl implements ActivationTokenService {

	private Log LOGGER = LogFactory.getLog(ActivationTokenServiceImpl.class);

	@Autowired
	private UserTokenRepository tokenRepo;

	@Autowired
	private JwtUserDetailsServiceImpl userService;

	@Autowired
	private EmailUtility emailUtility;

	@Value("${api.route.activation.path}")
	private String activationPath;
	
	@Value("${api.route.profile.changepassword}")
	private String changePasswordPath;

	@Autowired
	private HtmlUtility htmlUtility;

	@Autowired
	JwtUserDetailsServiceImpl userDetailsService;

	@Value("${server.application-path}")
	private String serverApplicationPath;

	/**
	 * Method to activate user based on the Token
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public String activateUser(String token) throws Exception {
		LOGGER.info("Activate User Service: Activate User");
		try {
			UserToken activationToken = tokenRepo.findByToken(token);
			String htmlBody = "Error in activating account";
			if (activationToken == null) {
				htmlBody = "Invalid Activation Code.";
				LOGGER.error("Activate User Service: Invalid Actvation Code ");
			} else {
				User user = userService.getUserByUserId(activationToken.getUser_id());
				if (user.getStatus().equals(UserStatusEnum.ACTIVE.getStatus())) {
					htmlBody = "User is already Activated.";
					LOGGER.debug("User is already Activated");
				}

				else {
					if (activationToken.isTokenExpired()) {
						htmlBody = "Token Expired";
						LOGGER.debug("Token has expired");
					} else if (activationToken.getUser_id() != null) {
						Boolean isActivated = userService.activateUser(activationToken.getUser_id());
						if (isActivated) {
							htmlBody = "Your account has been Activated.";
							activationToken.setTokenExpired(true);
							tokenRepo.save(activationToken);
							LOGGER.debug("Account has been activated for user: " + activationToken.getUser());
						}
					}
				}
			}
			String htmlContent = htmlUtility.createHtml(htmlBody);
			return htmlContent;
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}
	/*
	 * this method is used Activate User service: Create User Token
	 * 
	 */
	public UserToken createUserToken(HttpServletRequest request, User user,
			UserTokenTypeConstant userTokenTypeConstant) {
		LOGGER.info("Activate User service: Create User Token");
		UserToken createToken = new UserToken();
		createToken.setUser_id(user.get_id());
		createToken.setCreatedAT(new Date());
		createToken.setToken(UUID.randomUUID().toString());
		createToken.setType(userTokenTypeConstant.name());
		createToken.setTokenExpired(false);
		UserToken newUserToken = tokenRepo.save(createToken);
		if (newUserToken != null) {
			try {
				String link = null;
				String emailBody = null;
				if (userTokenTypeConstant == UserTokenTypeConstant.TOKEN_USER_CREATED) {
					link = new StringBuffer().append(ApplicationUtility.getCurrentUrl(request)).append("/")
							.append(serverApplicationPath.trim()).append("/").append(activationPath.trim()).append("/")
							.append(createToken.getToken()).toString();
					emailBody = "Please click on the following activation link <a href='" + link + "'>Activate</a>";
				} else if (userTokenTypeConstant == UserTokenTypeConstant.TOKEN_PASSWORD_RESET) {
					link = new StringBuffer().append(ApplicationUtility.getCurrentUrl(request)).append("/")
							.append(serverApplicationPath.trim()).append("/").append(changePasswordPath.trim())
							.append("/").append(createToken.getToken()).toString();

					emailBody = "Please click on the following reset link <a href='" + link + "'>Reset Password</a>";

				}
				emailUtility.sendMail(user.getEmail(), emailBody);
				LOGGER.info("Activate User service: Create User Token - Email sent to User: " + user.getEmail());

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return newUserToken;
	}
}
