package com.hubbox.rest.service;

import javax.servlet.http.HttpServletRequest;

import com.hubbox.rest.model.User;
import com.hubbox.rest.model.UserToken;

public interface ForgetPasswordService {

	/**
	 * @param user : stores the user with token
	 * @return
	 */
	public UserToken storeUserInfoWithPasswordToken(HttpServletRequest request, User user);
	
	/**
	 * @param token : token of the user
	 * @param tokenType : activation or reset password
	 * @return
	 */
	public UserToken findUserByTokenandType(String token, String tokenType);
	
	/**
	 * @param userToken : user token value
	 */
	public void saveUserTokenAfterPasswordChange(UserToken userToken);
	
}
