package com.hubbox.rest.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hubbox.constant.UserTokenTypeConstant;
import com.hubbox.rest.model.User;
import com.hubbox.rest.model.UserToken;
import com.hubbox.rest.repository.UserTokenRepository;

@Service
public class ForgetPasswordServiceImpl implements ForgetPasswordService {

	@Autowired
	private UserTokenRepository userTokenRepo;

	@Value("${server.application-path}")
	private String serverApplicationPath;
	
	@Autowired
	private ActivationTokenService activationTokenService;

	@Override
	public UserToken storeUserInfoWithPasswordToken(HttpServletRequest httpServletRequest, User user) {

		List<UserToken> listOfPreviousActiveTokens = userTokenRepo.findForgotPasswordTokensActive(user.get_id());
		if (!listOfPreviousActiveTokens.isEmpty()) {
			for (UserToken userToken : listOfPreviousActiveTokens) {
				userToken.setTokenExpired(true);
				userTokenRepo.save(userToken);
			}
		}
		return activationTokenService.createUserToken(httpServletRequest, user, UserTokenTypeConstant.TOKEN_PASSWORD_RESET);
	}

	/**
	 * @param token
	 *            : token of the user
	 * @param tokenType
	 *            : token type of the token issued to the user.
	 * 
	 *            DESCRIPTION : returns user info from token and token type.
	 */
	@Override
	public UserToken findUserByTokenandType(String token, String tokenType) {
		return userTokenRepo.findByTokenAndType(token, tokenType);
	}

	/**
	 * @param userToken : saves user token after password change
	 * */
	@Override
	public void saveUserTokenAfterPasswordChange(UserToken userToken) {
		userTokenRepo.save(userToken);
	}

}
