package com.hubbox.rest.service;

import javax.servlet.http.HttpServletRequest;

import com.hubbox.constant.UserTokenTypeConstant;
import com.hubbox.rest.model.User;
import com.hubbox.rest.model.UserToken;

/**
 * This is ActivationTokenService
 * 
 * @author Aquib Maniyar
 *
 */
public interface ActivationTokenService {

	public String activateUser(String token) throws Exception;

	public UserToken createUserToken(HttpServletRequest httpServletRequest, User user,  UserTokenTypeConstant userTokenTypeConstant);
}
