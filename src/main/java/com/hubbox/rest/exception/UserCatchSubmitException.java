package com.hubbox.rest.exception;

public class UserCatchSubmitException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an {@code UserCatchSubmitException} with the specified
	 * message and root cause.
	 *
	 * @param msg
	 *            the detail message
	 * @param t
	 *            the root cause
	 */
	public UserCatchSubmitException(String msg, Throwable t) {
		super(msg, t);
	}

	/**
	 * Constructs an {@code UserCatchSubmitException} with the specified
	 * message and no root cause.
	 *
	 * @param msg
	 *            the detail message
	 */
	public UserCatchSubmitException(String msg) {
		super(msg);
	}

}
