package com.hubbox.rest.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hubbox.util.ApplicationUtility;
import com.hubbox.util.ConversionUtility;
import com.hubbox.util.HttpStatusCodes;

/**
 * this class is helps to handle exception
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

	@Autowired
	ConversionUtility conversionUtility;

	@Autowired
	ApplicationUtility applicationUtility;

	/**
	 * this method handle exception
	 * 
	 * @param e
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<?> handleException(Exception e) {
		if (e instanceof MethodArgumentNotValidException) {
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.VALIDATION_ERROR,
					e.getMessage().substring(e.getMessage().lastIndexOf("; default message [") + 19,
							e.getMessage().length() - 3),
					null));
		} else {
			if (e.getMessage() == null)
				return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OTHER_EXCEPTION,
						applicationUtility.getMessage("other.exception.error"), null));
			else
				return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OTHER_EXCEPTION,
						e.getMessage(), null));
		}
	}

}