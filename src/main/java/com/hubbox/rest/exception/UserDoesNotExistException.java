package com.hubbox.rest.exception;

/**
 * This class helps to UserDoesNotExistException
 * 
 * @author Aquib
 * @version 1.0
 * @Date 14/06/2017
 *
 */
public class UserDoesNotExistException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an {@code UserDoesNotExistException} with the specified
	 * message and root cause.
	 *
	 * @param msg
	 *            the detail message
	 * @param t
	 *            the root cause
	 */
	public UserDoesNotExistException(String msg, Throwable t) {
		super(msg, t);
	}

	/**
	 * Constructs an {@code UserAlreadyExistException} with the specified
	 * message and no root cause.
	 *
	 * @param msg
	 *            the detail message
	 */
	public UserDoesNotExistException(String msg) {
		super(msg);
	}

}
