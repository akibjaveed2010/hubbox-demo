package com.hubbox.rest.exception;

/**
 * If user is already entered the Tournament.
 * @author amit
 *
 */
public class UserAlreadyRegisteredTournamentException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserAlreadyRegisteredTournamentException(String msg, Throwable t) {
		super(msg, t);
	}

	public UserAlreadyRegisteredTournamentException(String msg) {
		super(msg);
	}
}
