package com.hubbox.rest.exception;

/**
 * This class helps to UserAlreadyExistException
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public class UserTournamentAlreadyExistException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an {@code UserAlreadyExistException} with the specified
	 * message and root cause.
	 *
	 * @param msg
	 *            the detail message
	 * @param t
	 *            the root cause
	 */
	public UserTournamentAlreadyExistException(String msg, Throwable t) {
		super(msg, t);
	}

	/**
	 * Constructs an {@code UserAlreadyExistException} with the specified
	 * message and no root cause.
	 *
	 * @param msg
	 *            the detail message
	 */
	public UserTournamentAlreadyExistException(String msg) {
		super(msg);
	}

}
