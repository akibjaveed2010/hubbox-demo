package com.hubbox.rest.exception;

/**
 * This class helps to StorageException
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public class StorageException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public StorageException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
