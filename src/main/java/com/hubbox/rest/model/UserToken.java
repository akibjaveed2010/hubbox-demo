package com.hubbox.rest.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * this class is helps to created collection (userToken) by using following
 * field
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@Document(collection = "userToken")
public class UserToken {

	@Id
	private String _id;

	private String user_id;

	private String token;

	private String type;

	private Date createdAT;

	private boolean isTokenExpired;

	private String user;

	public String get_id() {
		return _id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreatedAT() {
		return createdAT;
	}

	public void setCreatedAT(Date createdAT) {
		this.createdAT = createdAT;
	}

	public String getUser() {
		return user;
	}

	public boolean isTokenExpired() {
		try {
			if (isTokenExpired) {
				return isTokenExpired;
			} else {
				long diffInDays = (System.currentTimeMillis() - this.createdAT.getTime()) / (1000 * 60 * 60 * 24);
				if (diffInDays > 30) {
					isTokenExpired = true;
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
		return isTokenExpired;
	}

	public void setTokenExpired(boolean isTokenExpired) {
		this.isTokenExpired = isTokenExpired;
	}

}