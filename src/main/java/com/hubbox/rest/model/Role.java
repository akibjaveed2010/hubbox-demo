package com.hubbox.rest.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * this class is helps to created authority collection having following fields
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@Document(collection = "authority")
public class Role {

	@Id
	private String _id;

	private String name;

	private Integer maxTournament;

	public Integer getMaxTournament() {
		return maxTournament;
	}

	public void setMaxTournament(Integer maxTournament) {
		this.maxTournament = maxTournament;
	}	

	public String getName() {
		return name;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public void setName(String name) {
		this.name = name;
	}

}