package com.hubbox.rest.model;

/**
 * @author amit
 *
 */
public class AssignedRole {
	private String authorityid;

	public AssignedRole(Role role) {
		this.authorityid = role.get_id();
		this.name = role.getName();
	}
	
	public AssignedRole() {
	}

	public String getAuthorityid() {
		return authorityid;
	}

	public String getName() {
		return name;
	}

	public void setAuthorityid(String authorityid) {
		this.authorityid = authorityid;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;
	
	

}
