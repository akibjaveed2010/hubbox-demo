package com.hubbox.rest.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class User {

	@Id
	private String _id;

	private String password;

	private String fullName;

	private String email;

	private String username;

	private String avatar;

	private String provider;

	private String dob;

	private String status;

	private List<AssignedRole> roles;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<AssignedRole> getRoles() {
		return roles;
	}

	public void setRoles(List<AssignedRole> roles) {
		this.roles = roles;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "User [_id=" + _id + ", password=" + password + ", fullName=" + fullName + ", email=" + email
				+ ", username=" + username + ", avatar=" + avatar + ", provider=" + provider + ", dob=" + dob
				+ ", status=" + status + ", roles=" + roles + "]";
	}

}