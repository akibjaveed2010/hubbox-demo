package com.hubbox.rest.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.hubbox.rest.model.UserToken;

/**
 * 
 * @author BhushanB
 *
 */
public interface UserTokenRepository extends MongoRepository<UserToken, Long> {

	@Query("{ 'token' : ?0 }")
	UserToken findByToken(String token);

	@Query("{ $and: [{'token' : ?0},{'type' : ?1}]}")
	UserToken findByTokenAndType(String token, String tokenType);
	
	@Query("{ $and: [ {'user_id': '?0'}, {'isTokenExpired': false}, {'type': 'TOKEN_PASSWORD_RESET' }] }")
	List<UserToken> findForgotPasswordTokensActive(String user_id);

}
