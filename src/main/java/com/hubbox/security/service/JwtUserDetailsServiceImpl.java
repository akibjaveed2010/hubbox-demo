package com.hubbox.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hubbox.constant.UserStatusEnum;
import com.hubbox.rest.model.User;
import com.hubbox.security.JwtUserFactory;
import com.hubbox.security.repository.UserRepository;

/**
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	MongoOperations mongoOperations;

	@Value("${tournaments.admin.permonth}")
	int tournamentsPerMonthLimitForAdmin;

	@Value("${tournaments.user.permonth}")
	int tournamentsPerMonthLimitForUser;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			return JwtUserFactory.create(user);
		}
	}

	public String getUserIdFromUserName(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			return user.get_id();
		}
	}

	/**
	 * @param username
	 * @return
	 * @throws UsernameNotFoundException
	 */
	public User getUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			return user;
		}
	}

	public User getUserByEmail(String email) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(email);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with email '%s'.", email));
		} else {
			return user;
		}
	}

	public boolean getUserByType(String type, String value) throws UsernameNotFoundException {
		User user = userRepository.findByType(type, value);

		if (user == null) {
			return false;
		} else {
			return true;
		}
	}

	public User getUserByUserId(String id) throws UsernameNotFoundException {
		User user = userRepository.findById(id);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with id '%s'.", id));
		} else {
			return user;
		}
	}

	public Boolean activateUser(String userId) {
		// Find user
		User user = userRepository.findById(userId);
		user.setStatus(UserStatusEnum.ACTIVE.getStatus());
		// Activate User
		User response = userRepository.save(user);
		if ((response instanceof User) && response.getStatus().equals(UserStatusEnum.ACTIVE.getStatus())) {
			return true;
		}
		return false;
	}

	public Boolean changePassword(String userId, String password) {
		User user = userRepository.findById(userId);
		user.setPassword(password);

		User response = userRepository.save(user);
		if ((response instanceof User)) {
			return true;
		}
		return false;
	}

}
