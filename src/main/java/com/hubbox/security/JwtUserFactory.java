package com.hubbox.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.hubbox.rest.model.Role;
import com.hubbox.rest.model.User;

/**
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public final class JwtUserFactory {

	private JwtUserFactory() {
	}

	/**
	 * @param user
	 * @return
	 */
	public static JwtUser create(User user) {
		return new JwtUser(user.get_id(), user.getEmail(), user.getFullName(), user.getPassword(),
				new ArrayList<GrantedAuthority>(), // mapToGrantedAuthorities(user.getAuthorities()),
				user.getStatus());

	}

	@SuppressWarnings("unused")
	private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> authorities) {
		return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getName()))
				.collect(Collectors.toList());
	}
}
