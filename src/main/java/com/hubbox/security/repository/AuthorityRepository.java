package com.hubbox.security.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.hubbox.rest.model.Role;

/**
 * This interface extend mongoRepository to do some database operations
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public interface AuthorityRepository extends MongoRepository<Role, Long> {
	
	@Query("{ 'name' : ?0 }")
	Role findByName(String name);

}
