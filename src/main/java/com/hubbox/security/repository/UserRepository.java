package com.hubbox.security.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.hubbox.rest.model.User;

/**
 * This interface extend mongoRepository to do some database operations
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public interface UserRepository extends MongoRepository<User, Long> {

	@Query("{ 'username' : ?0 }")
	User findByUsername(String username);

	@Query("{ 'email' : ?0 }")
	User findByEmail(String email);

	@Query("{ '?0' : ?1 }")
	User findByType(String type, String value);

	@Query("{ '_id' : ?0 }")
	User findById(String _id);

}
