package com.hubbox.controller;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hubbox.constant.ProviderTypeEnum;
import com.hubbox.constant.UserStatusEnum;
import com.hubbox.constant.UserTokenTypeConstant;
import com.hubbox.rest.model.User;
import com.hubbox.rest.model.UserToken;
import com.hubbox.rest.service.ForgetPasswordService;
import com.hubbox.security.JwtTokenUtil;
import com.hubbox.security.service.JwtUserDetailsServiceImpl;
import com.hubbox.util.ApplicationUtility;
import com.hubbox.util.EmailUtility;
import com.hubbox.util.HtmlUtility;
import com.hubbox.util.HttpStatusCodes;
import com.hubbox.util.ResponseConversionUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Changes the current password of the regeistered user
 * 
 * @author Vishal Arora
 * @version 1.0
 * @Date 12/06/2017
 * 
 *       reviewed by :
 * 
 */
@RestController
@Api(description = "Request for forgot password and save the new one through email")
public class ForgotPasswordRestController {

	private Log LOGGER = LogFactory.getLog(ForgotPasswordRestController.class);

	@Autowired
	private JwtUserDetailsServiceImpl userDetailsService;

	@Autowired
	private ForgetPasswordService forgetPasswordService;

	@Autowired
	private ResponseConversionUtility conversionUtility;

	@Autowired
	private ApplicationUtility applicationUtility;

	@Value("${api.route.profile.confirmpassword}")
	private String confirmPasswordPath;

	@Value("${api.route.profile.changepassword}")
	private String changePasswordPath;

	@Value("${reset.password.maxdelay}")
	private long resetPasswordMaxDelay;

	@Value("${server.application-path}")
	private String serverApplicationPath;

	@Autowired
	private EmailUtility emailUtility;

	@Autowired
	private HtmlUtility htmlUtility;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Value("${jwt.header}")
	private String tokenHeader;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private ResponseConversionUtility responseConversionUtility;

	/**
	 * @param email
	 *            : activated email id of the user
	 * @param request
	 *            : request from the user
	 * @return : notification to check email for reset link
	 * 
	 *         DESCRIPTION : Takes in an email parameter on which the mail is
	 *         supposed to be sent. if the account type is normal, which means
	 *         if the account is registered from the system, then it generates a
	 *         temporary user token and sends an email to the registered email
	 *         id for activation
	 * 
	 * @throws Exception
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@RequestMapping(value = "${api.route.profile.forgotpassword}", method = RequestMethod.POST)
	@ApiOperation(value = "Forgot Password", notes = "sends an email to the registered email id for resetting the password")
	public ResponseEntity<?> forgotPassword(@ApiParam("email id of the user") @RequestParam("email") String email,
			HttpServletRequest request) throws Exception {

		User user = userDetailsService.getUserByEmail(email);

		 if (user != null) {
				if (UserStatusEnum.ACTIVE.getStatus().compareTo(user.getStatus()) == 0) {
					if (user.getProvider().equals(ProviderTypeEnum.ACCOUNT_NORMAL.getStatus())) {

						UserToken forgotPassWordToken = forgetPasswordService.storeUserInfoWithPasswordToken(request, user);
						if (forgotPassWordToken != null) {

							return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OK,
									applicationUtility.getMessage("forgotpassword.reset.linksent", new String[] { email }),
									null));
						}
					} else if (user.getProvider().equals(ProviderTypeEnum.ACCOUNT_FACEBOOK.getStatus())) {
						return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OTHER_EXCEPTION,
								applicationUtility.getMessage("forgotpassword.reset.facebook", new String[] { email }),
								null));
					}
				} else {
					return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.NOT_ACTIVATED,
							applicationUtility.getMessage("login.inactive", new String[] { email }), null));
				}
			}
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.BAD_REQUEST,
					applicationUtility.getMessage("validation.email", new String[] { email }), null));
	}

	/**
	 * @param token
	 * @param request
	 * @return
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */
	@RequestMapping(value = "${api.route.profile.changepassword}/{token}", method = RequestMethod.GET)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Change Password", notes = "reenter the new password")
	public String changePassword(@ApiParam("token sent along with the link") @PathVariable("token") String token,
			HttpServletRequest request) throws MalformedURLException, URISyntaxException {
		
		UserToken resetToken = forgetPasswordService.findUserByTokenandType(token,
				UserTokenTypeConstant.TOKEN_PASSWORD_RESET.name());
		String authMsg = "Failed to reset password";
		if (resetToken == null) {
			authMsg = "You have requested to update password muliple times or after a long duration. Please request again. Thanks ... ";
		} else {
			if (resetToken.isTokenExpired()) {
				authMsg = "You have requested to update password muliple times or after a long duration. Please request again. Thanks ... ";
				LOGGER.debug("Token has expired");
			} else if (resetToken.getUser_id() != null) {
				long resetRequestTime = resetToken.getCreatedAT().getTime();
				long currentTime = System.currentTimeMillis();

				if ((currentTime - resetRequestTime) < resetPasswordMaxDelay) {
					String serverDetail = new StringBuffer().append(ApplicationUtility.getCurrentUrl(request))
							.append("/").append(serverApplicationPath.trim()).toString();

					String headerUrl = new StringBuffer().append(ApplicationUtility.getCurrentUrl(request)).append("/")
							.append(serverApplicationPath.trim()).append("/").append(confirmPasswordPath).toString();

					return htmlUtility.createForgetPasswordTemplate(serverDetail, headerUrl, token);
				} else {
					authMsg = "You have requested to update password muliple times or after a long duration. Please request again. Thanks ... ";
				}
			}
		}
		return htmlUtility.createPasswordAuthenticationBody(authMsg);

	}

	
	/**
	 * @param token
	 * @param pass
	 * @return
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RequestMapping(value = "${api.route.profile.confirmpassword}", method = RequestMethod.POST)
	@ApiOperation(value = "Confirm password", notes = "confirms the password and then changes it inside database")
	public String confirmPassword(
			@ApiParam("reset token sent along with the email") @RequestParam("resetToken") String token,
			@ApiParam("new password entered by the user") @RequestParam("pass") String pass) {
		UserToken resetToken = forgetPasswordService.findUserByTokenandType(token,
				UserTokenTypeConstant.TOKEN_PASSWORD_RESET.name());
		String authMsg = "Failed to reset password";
		if (resetToken == null) {
			authMsg = "You have requested to update password muliple times or after a long duration. Please request again. Thanks ... ";
		} else {

			User user = userDetailsService.getUserByUserId(resetToken.getUser_id());
			Boolean isPwdReset = userDetailsService.changePassword(user.get_id(), bCryptPasswordEncoder.encode(pass));
			if (isPwdReset) {
				authMsg = "Your password has been has been reset.";
				resetToken.setTokenExpired(true);
				forgetPasswordService.saveUserTokenAfterPasswordChange(resetToken);
			}
			return htmlUtility.createPasswordAuthenticationBody(authMsg);
		}

		return htmlUtility.createPasswordAuthenticationBody(authMsg);
	}
	

}
