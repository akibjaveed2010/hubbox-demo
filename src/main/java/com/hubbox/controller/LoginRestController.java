package com.hubbox.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hubbox.constant.UserStatusEnum;
import com.hubbox.rest.model.User;
import com.hubbox.security.JwtAuthenticationRequest;
import com.hubbox.security.JwtTokenUtil;
import com.hubbox.security.service.JwtUserDetailsServiceImpl;
import com.hubbox.util.ApplicationUtility;
import com.hubbox.util.HttpStatusCodes;
import com.hubbox.util.ResponseConversionUtility;
import com.hubbox.util.ValidationUtility;
import com.hubbox.util.WebSocketUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * this class helps listens to the Login request
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
@RestController
@Api(description = "Sign in the user inside system after validating its status")
public class LoginRestController {

	private Log LOGGER = LogFactory.getLog(LoginRestController.class);

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsServiceImpl userDetailsService;

	@Autowired
	private ResponseConversionUtility conversionUtility;

	@Autowired
	private ApplicationUtility applicationUtility;
	/*
	 * create a method createAthunticationToken and it returns
	 * 
	 */

	@Autowired
	private WebSocketUtility webSocketUtility;

	/**
	 * this method created AuthenticationToken by using following param
	 * 
	 * @param authenticationRequest
	 *            : username and password
	 * @param result
	 * @return token and it throws following exceptions
	 * @throws AuthenticationException
	 * @throws ParseException
	 */

	@RequestMapping(value = "${api.route.authentication.path}", method = RequestMethod.POST)
	@ApiOperation(value = "create token for login", notes = "creates authentication token for user at the time of login")
	public ResponseEntity<?> createAuthenticationToken(
			@ApiParam("user id and password of the user") @Valid @RequestBody JwtAuthenticationRequest authenticationRequest,
			BindingResult result) throws AuthenticationException, ParseException {
		LOGGER.info("Login Controller Login: START");
		if (result.hasErrors()) {
			LOGGER.error("Create Authentication Token: Validation Errors");
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.VALIDATION_ERROR,
					result.getAllErrors().get(0).getDefaultMessage(), null));
		} else if (!ValidationUtility.isValidEmailAddress(authenticationRequest.getUsername())) {
			LOGGER.error("Create Authentication Token: Invalid EmailAddress");
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.VALIDATION_ERROR,
					applicationUtility.getMessage("validation.email"), null));
		} else {
			// form input is ok
			try {
				LOGGER.info("Login Controller: " + authenticationRequest.getUsername());
				// Perform the security
				final Authentication authentication = authenticationManager
						.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
								authenticationRequest.getPassword()));
				SecurityContextHolder.getContext().setAuthentication(authentication);

				final User user = userDetailsService.getUserByEmail(authenticationRequest.getUsername());

				final String token = jwtTokenUtil.generateToken(user);
				// Return the token
				if (!user.getStatus().equals(UserStatusEnum.ACTIVE.getStatus())) {
					LOGGER.debug("Login Controller: " + authenticationRequest.getUsername() + " not activated");
					return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.NOT_ACTIVATED,
							applicationUtility.getMessage("login.inactive"), null));
				} else {
					LOGGER.debug("Login Controller: " + authenticationRequest.getUsername() + " is active user");
					return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OK,
							applicationUtility.getMessage("login.success"),
							conversionUtility.successfullyLoggedIn(user, token)));
				}
			} catch (BadCredentialsException BadCredentialsException) {
				LOGGER.error("Login Controller: " + authenticationRequest.getUsername() + " - Bad credentials");
				return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.BAD_REQUEST,
						applicationUtility.getMessage("login.invalidcredentails"), null));
			}
		}
	}

}
