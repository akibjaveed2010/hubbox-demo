package com.hubbox.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hubbox.constant.ProviderTypeEnum;
import com.hubbox.constant.UserTokenTypeConstant;
import com.hubbox.rest.exception.UserAlreadyExistException;
import com.hubbox.rest.model.User;
import com.hubbox.rest.request.model.RegisterDTO;
import com.hubbox.rest.service.ActivationTokenService;
import com.hubbox.rest.service.RegisterService;
import com.hubbox.security.JwtTokenUtil;
import com.hubbox.util.ApplicationUtility;
import com.hubbox.util.HttpStatusCodes;
import com.hubbox.util.ResponseConversionUtility;
import com.hubbox.util.ValidationUtility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * This class helps listens to the register request
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 */
@RestController
@Api(description = "registers a new user based on account type")
public class RegisterRestController {

	private Log LOGGER = LogFactory.getLog(RegisterRestController.class);

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private RegisterService registerService;

	@Autowired
	private ResponseConversionUtility conversionUtility;

	@Autowired
	private ApplicationUtility applicationUtility;

	@Autowired
	private ActivationTokenService activationTokenService;

	/**
	 * this method is used to register user by using registerType and following
	 * parameter
	 * 
	 * @param registerDTO : fullname, username, email and password
	 * @param registerType : 1 or 2
	 * @param httpServletRequest
	 * @param result
	 *            it throws following exception
	 * @throws AuthenticationException
	 */
	@RequestMapping(value = "${api.route.register.path}/{registerType}", method = RequestMethod.POST)
	@ApiOperation(value = "Register User", notes = "Takes the mandatory parameters like username, email, password and full name. Also asks for account type")
	public ResponseEntity<?> registerUser(@ApiParam("username, fullname, email and password") @Valid @RequestBody RegisterDTO registerDTO,
			@ApiParam("account type differentiating between facebook and email signup") @PathVariable("registerType") int registerType, HttpServletRequest httpServletRequest, BindingResult result)
			throws AuthenticationException {
		LOGGER.info("Register User");
		if (result.hasErrors()) {
			// form validation error
			LOGGER.debug("Register User: Invalid input");
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.VALIDATION_ERROR,
					result.getAllErrors().get(0).getDefaultMessage(), null));
		} else if (!ValidationUtility.isValidEmailAddress(registerDTO.getEmail())) {
			LOGGER.debug("Register User: Invalid Email Address");
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.VALIDATION_ERROR,
					applicationUtility.getMessage("validation.email"), null));
		}else if(registerDTO.getUsername().trim() == "" || registerDTO.getFull_name().trim() == "" || registerDTO.getPassword().trim() == ""){
			return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.VALIDATION_ERROR,
					applicationUtility.getMessage("validation.empty"), null));
		}	
		else {
			// form input is ok
			try {
				User user = registerService.registerUser(registerDTO, registerType);
				LOGGER.debug("Registered User: " + user.getEmail());
				/*
				 * checks if the account type is normal if yes then generates an
				 * activation token for the user to activate.
				 * 
				 * else if the account type is 'facebook', then generates a new
				 * user that is already activated.
				 */
				if (user.getProvider().equals(ProviderTypeEnum.ACCOUNT_NORMAL.getStatus())) {
					activationTokenService.createUserToken(httpServletRequest, user ,  UserTokenTypeConstant.TOKEN_USER_CREATED);
					LOGGER.debug("Registered User: Created Activation Token");
					return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OK,
							applicationUtility.getMessage("register.success"), null));
				} else if (user.getProvider().equals(ProviderTypeEnum.ACCOUNT_FACEBOOK.getStatus())) {
					final Authentication authentication = authenticationManager.authenticate(
							new UsernamePasswordAuthenticationToken(registerDTO.getEmail(), registerDTO.getPassword()));
					SecurityContextHolder.getContext().setAuthentication(authentication);

					final String token = jwtTokenUtil.generateToken(user);
					return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OK,
							applicationUtility.getMessage("login.success"),
							conversionUtility.successfullyLoggedIn(user, token)));
				} else {
					return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OTHER_EXCEPTION,
							applicationUtility.getMessage("register.failed"), null));
				}

			} catch (UserAlreadyExistException e) {
				LOGGER.error("Registered User: UserAlreadyExistException " + e.getMessage());
				return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.REGISTERED,
						e.getLocalizedMessage(), null));
			} catch (Exception e) {
				LOGGER.error("Registered User: Exception " + e.getMessage());
				return ResponseEntity.ok(conversionUtility.createResponseEntityDTO(HttpStatusCodes.OTHER_EXCEPTION,
						e.getMessage(), null));
			}
		}
	}

}