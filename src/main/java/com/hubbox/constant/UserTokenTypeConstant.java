package com.hubbox.constant;

/**
 * this class having two UserTokenType Constant
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */
public enum UserTokenTypeConstant {

	TOKEN_USER_CREATED, TOKEN_PASSWORD_RESET, TOKEN_EMAIL_CHANGE;
}
