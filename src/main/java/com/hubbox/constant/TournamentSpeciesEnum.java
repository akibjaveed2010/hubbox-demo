package com.hubbox.constant;

/**
 * @author Vishal Arora
 *
 *species of fishes that can be submitted
 */
public enum TournamentSpeciesEnum {

	ANY(1, "any"),
	BASS_ONLY(2, "bass_only"),
	LARGEMOUTH_ONLY(3, "bass_only"),
	SPOTTED_ONLY(4, "spotted_only"),
	SMALLMOUTH_ONLY(5, "smallmouth_only"),
	TROUT_ONLY(6, "trout_only"),
	CATFISH_ONLY(7, "catfish_only"),
	PANFISH_ONLY(8, "panfish_only");
	
	private int species_code;
	
	private String species_name;

	public int getSpecies_code() {
		return species_code;
	}

	public void setSpecies_code(int species_code) {
		this.species_code = species_code;
	}

	public String getSpecies_name() {
		return species_name;
	}

	public void setSpecies_name(String species_name) {
		this.species_name = species_name;
	}

	private TournamentSpeciesEnum(int species_code, String species_name) {
		this.species_code = species_code;
		this.species_name = species_name;
	}	
	
}
