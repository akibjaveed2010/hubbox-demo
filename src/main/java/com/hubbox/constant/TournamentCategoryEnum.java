package com.hubbox.constant;

/**
 * this class having two account types
 * 
 * @author Aquib
 * @version 1.0
 * @Date 06/06/2017
 *
 */

public enum TournamentCategoryEnum {

	PUBLIC(1, "PUBLIC"), PRIVATE(2, "PRIVATE");

	private int statusCode;

	private String status;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private TournamentCategoryEnum(int userStatusCode, String status) {

		this.statusCode = userStatusCode;
		this.status = status;
	}

}
