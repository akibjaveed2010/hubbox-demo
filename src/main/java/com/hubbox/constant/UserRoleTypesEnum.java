package com.hubbox.constant;

public enum UserRoleTypesEnum {
	ROLE_SUPER_ADMIN(0, "ROLE_SUPER_ADMIN"), 
	ROLE_TOURNAMENT_ADMIN(1, "ROLE_TOURNAMENT_ADMIN"), 
	ROLE_TOURNAMENT_REVIEWER(2, "ROLE_TOURNAMENT_REVIEWER"), 
	ROLE_USER(3, "ROLE_USER");

	private int userRoleCode;
	private String userRoleName;

	public int getUserRoleCode() {
		return userRoleCode;
	}

	public void setUserRoleCode(int userRoleCode) {
		this.userRoleCode = userRoleCode;
	}

	public String getUserRoleName() {
		return userRoleName;
	}

	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}

	private UserRoleTypesEnum(int userRoleCode, String userRoleName) {
		this.userRoleCode = userRoleCode;
		this.userRoleName = userRoleName;
	}

}
