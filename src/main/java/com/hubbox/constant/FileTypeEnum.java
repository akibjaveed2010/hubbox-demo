package com.hubbox.constant;

/**
 * Enum for File Type status.
 * 
 * @author Aquib
 *
 */
public enum FileTypeEnum {

	USER_PROFILE(0, "userprofile", "profile"), TOURNAMENT_LOGO(1, "tournamentlogo", "logo"), TOURNAMENT_PRIZE(2,
			"tournamentprize", "prize"), CATCH_IMAGE(3, "catchimage", "image"), CATCH_VIDEO(4, "catchvideo", "video");
	private int fileCode;
	private String fileType;
	private String fileFolder;

	public String getFileFolder() {
		return fileFolder;
	}

	public void setFileFolder(String fileFolder) {
		this.fileFolder = fileFolder;
	}

	public int getFileCode() {
		return fileCode;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileCode(int fileCode) {
		this.fileCode = fileCode;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	private FileTypeEnum(int fileCode, String fileType, String fileFolder) {
		this.fileCode = fileCode;
		this.fileType = fileType;
		this.fileFolder = fileFolder;
	}

}
