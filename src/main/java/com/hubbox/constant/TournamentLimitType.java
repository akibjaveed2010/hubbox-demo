package com.hubbox.constant;

/**
 * @author Vishal Arora
 * 
 * limit types classified on creation of tournament
 *
 */
public enum TournamentLimitType {

	BIGFISH(1, "bigfish"), THREE_FISH(2, "3fish"), FIVE_FISH(3, "5fish"), UNLIMITED(4, "unlimited");

	private int limitTypeCode;

	private String limitTypeValue;

	public int getLimitTypeCode() {
		return limitTypeCode;
	}

	public void setLimitTypeCode(int limitTypeCode) {
		this.limitTypeCode = limitTypeCode;
	}

	public String getLimitTypeValue() {
		return limitTypeValue;
	}

	public void setLimitTypeValue(String limitTypeValue) {
		this.limitTypeValue = limitTypeValue;
	}

	private TournamentLimitType(int limitTypeCode, String limitTypeValue) {
		this.limitTypeCode = limitTypeCode;
		this.limitTypeValue = limitTypeValue;
	}

}
