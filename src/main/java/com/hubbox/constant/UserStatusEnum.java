package com.hubbox.constant;

/**
 * Enum for User status.
 * 
 * @author amit
 *
 */
public enum UserStatusEnum {

	NEW(0, "new"), ACTIVE(1, "active"), IN_ACTIVE(2, "incative");

	private int userStatusCode;

	public int getUserStatusCode() {
		return userStatusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setUserStatusCode(int userStatusCode) {
		this.userStatusCode = userStatusCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String status;

	private UserStatusEnum(int userStatusCode, String status) {

		this.userStatusCode = userStatusCode;
		this.status = status;
	}
}
