package com.hubbox.constant;

/**
 * Enum for tournament activity type
 * 
 * @author Venkatesh
 * @version 1.0
 * @Date 28/06/2017
 */
public enum TournamentActivityTypeEnum {

	JOIN(1, "JOIN"), CATCH(2, "CATCH"), WIN(3, "WIN");

	private int activityStatusCode;
	private String activityType;

	private TournamentActivityTypeEnum(int activityStatusCode, String activityType) {
		this.activityStatusCode = activityStatusCode;
		this.activityType = activityType;
	}

	public int getActivityStatusCode() {
		return activityStatusCode;
	}

	public void setActivityStatusCode(int activityStatusCode) {
		this.activityStatusCode = activityStatusCode;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
}
