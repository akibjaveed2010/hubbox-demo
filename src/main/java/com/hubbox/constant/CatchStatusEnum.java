package com.hubbox.constant;

/**
 * Enum for Catch status.
 * 
 * @author Aquib
 *
 */
public enum CatchStatusEnum {

	REVIEW(0, "review"), VERIFIED(1, "verified"), UNVERIFIED(2, "unverified"), DECLINE(3, "decline"), NOACTION(4,"noaction");

	private int catchStatusCode;

	public int getCatchStatusCode() {
		return catchStatusCode;
	}

	public void setCatchStatusCode(int catchStatusCode) {
		this.catchStatusCode = catchStatusCode;
	}

	private String catchStatus;

	public String getCatchStatus() {
		return catchStatus;
	}

	public void setCatchStatus(String catchStatus) {
		this.catchStatus = catchStatus;
	}

	private CatchStatusEnum(int catchStatusCode, String catchStatus) {

		this.catchStatusCode = catchStatusCode;
		this.catchStatus = catchStatus;
	}
}
