package com.hubbox.constant;

/**
 * @author Vishal Arora
 * 
 * entry fees and their values
 *
 */
public enum TournamentEntryFeesEnum {
	
	FREE(1, 0),
	FIVE(2, 5),
	TEN(3, 10),
	FIFTEEN(4, 15),
	TWENTY(5, 20),
	TWENTY_FIVE(6, 25),
	THIRTY(7, 30),
	THIRTY_FIVE(8, 35),
	FORTY(9, 40),
	FORTY_FIVE(10, 45),
	FIFTY(11, 50),
	SEVENTY_FIVE(12, 75),
	HUNDRED(13, 100);
	
	private int entryFeesCode;
	
	private double entryFeesValue;

	public int getEntryFeesCode() {
		return entryFeesCode;
	}

	public void setEntryFeesCode(int entryFeesCode) {
		this.entryFeesCode = entryFeesCode;
	}

	public double getEntryFeesValue() {
		return entryFeesValue;
	}

	public void setEntryFeesValue(double entryFeesValue) {
		this.entryFeesValue = entryFeesValue;
	}

	private TournamentEntryFeesEnum(int entryFeesCode, double entryFeesValue) {
		this.entryFeesCode = entryFeesCode;
		this.entryFeesValue = entryFeesValue;
	}	

}
