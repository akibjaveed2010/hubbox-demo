package com.hubbox.constant;
/**
 * this class having four tournament feed typeS eg.PAST,LIVE,LIVE_JOINED,FUTURE.
 * 
 * @author venkatesh
 * @version 1.0
 * @Date 22/06/2017
 *
 */

public enum TournamentTimeFrameEnum {

	PAST(1, "PAST"), LIVE(2, "LIVE"), LIVE_JOINED(3, "LIVE_JOINED"), FUTURE(4, "FUTURE");

	private int code;
	private String status;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private TournamentTimeFrameEnum(int code, String status) {
		this.code = code;
		this.status = status;
	}

}
