#1.Example to deploy code - ./developer.sh deploy
#2.Example to deploy code with git pull - ./developer.sh deploy git

if [[ $2 = "git" ]]; then
git pull origin service-dev
fi
if [[ $1 = "debug" ]]; then
export MAVEN_OPTS=-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n;
mvn clean install
mvn clean tomcat7:run
elif [[ $1 = "dev" ]]; then
mvn clean install -Dmaven.test.skip=true;
elif [[ $1 = "prod" ]]; then
mvn clean install
mvn clean tomcat7:run
elif [[ $1 = "deploy" ]]; then
sh /opt/tomcat/apache-tomcat-8.0.42/bin/shutdown.sh
mvn clean install
sudo rm -rf /opt/tomcat/apache-tomcat-8.0.42/webapps/hubbox-1.0.0.war 
sudo rm -rf /opt/tomcat/apache-tomcat-8.0.42/webapps/hubbox-1.0.0
sudo cp target/hubbox-1.0.0.war /opt/tomcat/apache-tomcat-8.0.42/webapps/
sh /opt/tomcat/apache-tomcat-8.0.42/bin/startup.sh 
fi
